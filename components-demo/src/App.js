import React, {Component} from 'react';
import './App.css';

class App extends Component {
    state = {
        number: [
            {first: 0},
            {second: 0},
            {third: 0},
            {fourth: 0},
            {fifth: 0}
        ]
    }

    getNumbers = () => {
        let random_start = 5;
        let random_end = 36;
        let allСycles = 5;
        let array= []
        let numbers = []
        for(let i=random_start;i<=random_end;i++){
            array.push(i)
        }
        for(let countCycles=1;countCycles<=allСycles;countCycles++){
            numbers.push(array.splice(Math.random()*array.length,1)[0])
        }
        function compareNumbers(a, b) {
            return a - b;
        }
        let first = numbers.sort(compareNumbers)[0]
        let second = numbers.sort(compareNumbers)[1]
        let third = numbers.sort(compareNumbers)[2]
        let fourth = numbers.sort(compareNumbers)[3]
        let fifth = numbers.sort(compareNumbers)[4]
        this.setState(
            {
                number: [
                    {first: first},
                    {second: second},
                    {third: third},
                    {fourth: fourth},
                    {fifth: fifth}
                ]
            }
        )
    }
    render() {
        return (
            <div>
                <button type="button" onClick={this.getNumbers}>New numbers</button>
                <div className="circles">
                    <div className="circle"><p className="number">{this.state.number[0].first}</p></div>
                    <div className="circle"><p className="number">{this.state.number[1].second}</p></div>
                    <div className="circle"><p className="number">{this.state.number[2].third}</p></div>
                    <div className="circle"><p className="number">{this.state.number[3].fourth}</p></div>
                    <div className="circle"><p className="number">{this.state.number[4].fifth}</p></div>
                </div>
            </div>
        );
    }
}
export default App;
